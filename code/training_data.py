"""Prepare Training Data for a ML Model"""

import spacy
import json
import prepare_data
import re
import random
from cleanerClass import Cleaner
from faker import Faker



def generate_tests(codes, names):
    # Load Data
    fake = Faker()
    tests = []
    types_ = ['A','B','C','D','E','F','G','H']
    
    for i in range(len(codes)-5):

        if types_[0]=='A':
            # QUANTIY AND CODE
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " +str(fake.random_int(0, 10000)) +" " +str(codes[i])+ random.choice([",","--","|"]) + " " + str(fake.random_int(0, 10000))+ " " +str(codes[i+1]) + " " +sent2 + " " + str(fake.random_int(0, 10000)) + " " +str(codes[i+2])
            tests.append(inpt)


        if types_[1]=='B':
            # QUANTITY AND NAME
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent3 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " +str(fake.random_int(0, 10000)) + " " +str(names[i]) + " " + sent2 + " " +str(fake.random_int(0, 10000)) + " " +str(names[i+1])+random.choice([",","--","|"])+ " " +str(fake.random_int(0, 10000)) + " " +str(names[i+2]) + sent3
            tests.append(inpt)   


        if types_[2]=='C':
            # "CODE AND QUANTITY
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent3 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent3+ " "+ str(codes[i+2]) + " " + str(fake.random_int(0, 10000)) + " " +sent1 + " " + str(codes[i]) + " " + str(fake.random_int(0, 10000)) + " " + sent2 + " "+str(codes[i+1])+ " "+ str(fake.random_int(0, 10000))+ random.choice([",","--","|"]) + " " +str(codes[i+1])+ " "+ str(fake.random_int(0, 10000)) 
            tests.append(inpt)  

            
        if types_[3]=='D':
            # "NAME AND QUANTITY"
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent3 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " + str(names[i]) + " " + str(fake.random_int(0, 10000)) + " " + sent2 + " " + str(names[i+1]) + " " + str(fake.random_int(0, 10000)) + random.choice([",","--","|"]) + " " + str(names[i+4]) + " " + str(fake.random_int(0, 10000)) +  " " + sent3
            tests.append(inpt)      
            


        if types_[4]=='E':
            # QUANTITY, CODE AND NAME
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " + str(fake.random_int(0, 10000)) + " " + str(codes[i]) + " " + str(names[i])+ random.choice([",","--","|"]) + " " + str(fake.random_int(0, 10000)) + " " + str(codes[i+1]) + " " + str(names[i+1]) + " " + sent2 + " " + str(fake.random_int(0, 10000)) + " " + str(codes[i+2]) + " " + str(names[i+2])+ random.choice([",","--","|"]) + " " + str(fake.random_int(0, 10000)) + " " + str(codes[i+3]) + " " + str(names[i+3])
            tests.append(inpt)
  
              
        if types_[5]=='F':
             # QUANTITY, NAME AND CODE
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " + str(fake.random_int(0, 10000))+ " " + str(names[i]) + " " + str(codes[i])+ random.choice([",","--","|"]) + " " + str(fake.random_int(0, 10000))+ " " + str(names[i+4]) + " " + str(codes[i+4])  + " " + sent2 + " " + str(fake.random_int(0, 10000))+ " " + str(names[i+3]) + " " + str(codes[i+3])
            tests.append(inpt)  
               


        if types_[6]=='G':
            # NAME, CODE AND QUANTITY
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " + str(names[i]) + " " + str(codes[i]) + " " + str(fake.random_int(0, 10000))  + " " + sent2 + " " + str(names[i+2]) + " " + str(codes[i+2]) + " " + str(fake.random_int(0, 10000))
            tests.append(inpt)  
    

        if types_[7]=='H':
            # CODE, NAME AND QUANTITY
            sent1 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent2 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            sent3 = prepare_data.clean(fake.paragraph(nb_sentences=random.randint(1, 10)))
            inpt = sent1 + " " + str(codes[i]) + " " + str(names[i]) + " " + str(fake.random_int(0, 10000)) + random.choice([",","-","|"]) +"\n" + str(codes[i+2]) + " " + str(names[i+2]) + " " + str(fake.random_int(0, 10000)) +" " + sent2  + " " + str(codes[i+1]) + " " + str(names[i+1]) + " " + str(fake.random_int(0, 10000)) 
            tests.append(inpt)   
          
    return tests

# Test Model
def test_model(model, tests, dt):
    for test in tests:
        clean_text = prepare_data.clean(test)
        doc = model(clean_text)
        results = ()
        ents = [(ent.start_char, ent.end_char, ent.label_) for ent in doc.ents]
        if len(ents) >0:
            results = (clean_text, {"entities":ents})
            dt.append(results)
 

def save_data(file, data):
    with open(file, "w", encoding="utf-8") as f:
        json.dump(data, f, indent=4)


def main():
    cleaner = Cleaner()
    nlp = spacy.load('./models/initial_model')               # Load Initial Model 
    DATA = []
    codes, names = cleaner.load_data("./data/save.csv")    # File with codes, and names
    tests = generate_tests(codes, names)  # Create TEST Cases
    test_model(nlp, tests, DATA)
    random.shuffle(DATA)

    # 90% of data for training, 10% for testing
    TRAIN_DATA = DATA[ :int(len(DATA)*0.9)]
    TEST_DATA = DATA[int(len(DATA)*0.9):]

    save_data("./data/TRAIN_DATA.json", TRAIN_DATA)
    save_data("./data/TEST_DATA.json", TEST_DATA)
 

  

if __name__ == '__main__':
    main()

