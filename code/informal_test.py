import spacy
import json
from cleanerClass import Cleaner
import re

def save_data(doc):
    results = []
    counter = 1
    
    for ent in doc.ents:

        if ent.label_ == "QUANTITY AND CODE":
            result = {"LABEL":ent.label_,"QUANTITY":0,"CODE":""}
            gap = ent.text.find(" ")
            result["QUANTITY"] = int(ent.text[:gap])
            result["CODE"] =ent.text[gap+1:]
            results.append({"Item"+str(counter):result})
            counter= counter +1


        elif ent.label_ == "CODE AND QUANTITY":
            result = {"LABEL": ent.label_,"QUANTITY":0,"CODE":""}
            gap = ent.text.find(" ")
            result["CODE"] = ent.text[:gap]
            result["QUANTITY"] = int(ent.text[gap+1:])
            results.append({"Item"+str(counter):result})
            counter= counter +1

        elif ent.label_ == "QUANTITY AND NAME":
            result = {"LABEL": ent.label_,"QUANTITY":0, "NAME":""}
            gap = ent.text.find(" ")
            result["QUANTITY"] = int(ent.text[:gap])
            result["NAME"] = ent.text[gap+1:]
            results.append({"Item"+str(counter):result})
            counter= counter +1

        elif ent.label_ == "NAME AND QUANTITY":
            result = {"LABEL": ent.label_,"QUANTITY":0, "NAME":""}
            word = ent.text
            rf = re.compile('(\d+)(?!.*\d)')
            matches = re.finditer(rf,word)
            matches2 = re.findall(rf,word)
            for m , n in zip(matches, matches2):
                    # print(m.span())
                result["QUANTITY"] = int(n)
                word = word.replace(n, "") 
            result["NAME"] = word.strip()
            results.append({"Item"+str(counter):result})          
            counter= counter +1

        elif ent.label_ == "CODE, NAME AND QUANTITY":
            result = {"LABEL": ent.label_,"QUANTITY":0, "CODE":"", "NAME":"", }
            word = ent.text
            gap = word.find(" ")
            rf = re.compile('(\d+)(?!.*\d)')
            matches = re.finditer(rf,word)
            matches2 = re.findall(rf,word)
            code = word[:gap]
            result["CODE"] =code
            word = word.replace(code, "")
            for m , n in zip(matches, matches2):
                # print(m.span())
                result["QUANTITY"] = int(n)
                word = word.replace(n, "") 
            result["NAME"] = word.strip()
            results.append({"Item"+str(counter):result})            
            counter+=1

        elif ent.label_ == "NAME, CODE AND QUANTITY":
            result = {"LABEL": ent.label_,"QUANTITY":0, "CODE":"", "NAME":"", }
            word = ent.text
            rf = re.compile('(\d+)(?!.*\d)')
            rf2 = re.compile('\w{2}\S\d+')

            matches = re.finditer(rf,word)
            matches2 = re.findall(rf,word)
        
            Matches = re.finditer(rf2,word)
            Matches2 = re.findall(rf2,word)

            for m , n in zip(matches, matches2):
                # print(m.span())
                result["QUANTITY"] = int(n)
                word = word.replace(n, "")

            for m , n in zip(Matches, Matches2):
                # print(m)
                result["CODE"] = n
                word = word.replace(n, "")

            result["NAME"] = word.strip()
            results.append({"Item"+str(counter):result})
            counter+=1
            

        elif ent.label_ == "QUANTITY, NAME AND CODE":
            result = {"LABEL": ent.label_,"QUANTITY":0, "CODE":"", "NAME":"", }
            word = ent.text
            gap = ent.text.find(" ")
            quant = ent.text[:gap]
            result["QUANTITY"] = int(quant)
            word = word.replace(quant, "")
            rf2 = re.compile('\w{2}\S\d+')    
            Matches = re.finditer(rf2,word)
            Matches2 = re.findall(rf2,word)

            for m , n in zip(Matches, Matches2):
                # print(m)
                result["CODE"] = n
                word = word.replace(n, "")

            result["NAME"] = word.strip()  
            results.append({"Item"+str(counter):result})
            counter+=1
            

        elif ent.label_ == "QUANTITY, CODE AND NAME":
            result = {"LABEL": ent.label_,"QUANTITY":0, "CODE":"", "NAME":"", }
            word = ent.text
            gap = ent.text.find(" ")
            quant = ent.text[:gap]
            result["QUANTITY"] = int(quant)
            word = word.replace(quant, "")
            rf2 = re.compile('\w{2}\S\d+')    
            Matches = re.finditer(rf2,word)
            Matches2 = re.findall(rf2,word)

            for m , n in zip(Matches, Matches2):
                # print(m)
                result["CODE"] = n
                word = word.replace(n, "")

            result["NAME"] = word.strip()
            results.append({"Item"+str(counter):result})
            counter+=1
    with open("../data/results.json", "w", encoding="utf-8") as f:
        json.dump(results, f, indent=4)
    return results



def main():
        cleaner  = Cleaner()
        # nlp = spacy.load("./models/model-best")
        nlp = spacy.load("./models/model-last")

        text1 = "I would like to buy:  \nAA250 329, \n2000 CK100, 800 CK102, \n200 CK106 and CG200 2000."  # Q + C
        text2 = "I would like to buy:  \nAX250 329, \n2000 Angelica keiskei, \n800 Tilia argentea."  # Q + N
        text3 = "I would like to buy:  \nAA250 329, \n2000 CK100 Angelica keiskei, 800 CK102 Tilia argentea and 200 CK106 \nAllium porrum."  # Q + C + N

        # clean_text = prepare_data.clean(text1)
        # clean_text = prepare_data.clean(text2)
        clean_text = cleaner.clean(text3)
        print("\nCleaned Order Text: "+clean_text+"\n")
        doc = nlp(clean_text)
        # for ent in doc.ents:
        #     print(ent.text, ent.label_)
        results = save_data(doc)
        for result in results:
            print(result)


if __name__ == '__main__':
    main()