""" Create Intial Model and Patterns
"""
import spacy
from spacy.lang.en import English
import json
import re
import pandas as pd
from cleanerClass import Cleaner


def load_data(file):
    """Load data and extract"""

    data= pd.read_csv(file, encoding="utf-8", sep=';')
    codes = list(data["Column1"].unique()[1:])
    names = list(data["Column3"].unique()[1:])
    return (codes, names)



def create_patterns(codes, names):
    """Create Patterns"""

    ls = []
    types_ = ['A','B','C','D','E','F','G','H']
    counter = 0

    for it in range(len(codes)):
        names2 = names[it].lower().split()

        one_word = []
        NUM_DICT = {"TEXT":{"REGEX":r'^[0-9]+$'}}
        CODE_DICT = {"LOWER":str(codes[it]).lower()}
        pattern1 = {}
        pattern2 = {}
        pattern3 = {}
        pattern4 = {}
        pattern5 = {}
        pattern6 = {}
        pattern7 = {}
        pattern8 = {}

        for name in names2:
            word_dict = {"LOWER":name}
            one_word.append(word_dict)

        if types_[7]=='H':
            pat = []
            pat.append(CODE_DICT)
            pat = pat + one_word
            pat.append(NUM_DICT)    
            pattern8 = {"label": "CODE, NAME AND QUANTITY", "pattern": pat, "id":"cloudH"}

        if types_[6]=='G':
            pat = [] 
            pat = pat + one_word 
            pat.append(CODE_DICT)
            pat.append(NUM_DICT)  
            pattern7 = {"label": "NAME, CODE AND QUANTITY", "pattern": pat, "id":"cloudG"}

        if types_[0]=='A':
            pat = []
            pat.append(NUM_DICT)
            pat.append(CODE_DICT)
            pattern1 = {"label": "QUANTITY AND CODE", "pattern": pat, "id":"cloudA"}

       

        if types_[1]=='B':
            pat = []
            pat.append(NUM_DICT)
            pat = pat + one_word    
            pattern2 = {"label": "QUANTITY AND NAME", "pattern": pat, "id":"cloudB"}
            
        

        if types_[2]=='C':
            pat = []
            pat.append(CODE_DICT)
            pat.append(NUM_DICT)   
            pattern3 = {"label": "CODE AND QUANTITY", "pattern": pat, "id":"cloudC"}

           
        if types_[3]=='D':
            pat = []
            pat = pat + one_word 
            pat.append(NUM_DICT)     
            pattern4 = {"label": "NAME AND QUANTITY", "pattern": pat, "id":"cloudD"}
            

        if types_[4]=='E':
            pat = []
            pat.append(NUM_DICT)
            pat.append(CODE_DICT)
            pat = pat + one_word
            pattern5 = {"label": "QUANTITY, CODE AND NAME", "pattern": pat, "id":"cloudE"}


        if types_[5]=='F':
            pat = [] 
            pat.append(NUM_DICT)
            pat = pat + one_word
            pat.append(CODE_DICT)   
            pattern6 = {"label": "QUANTITY, NAME AND CODE", "pattern": pat, "id":"cloudF"}
            
        ls.append(pattern8)
        ls.append(pattern7)                                                                                     
        ls.append(pattern3)
        ls.append(pattern4)
        ls.append(pattern1)
        ls.append(pattern2)
        ls.append(pattern5)
        ls.append(pattern6)

    return ls


# Create Model and save model
def create_model(patterns):
    nlp = English()
    ruler = nlp.add_pipe("entity_ruler",validate = True) 
    ruler.add_patterns(patterns)
    nlp.to_disk("./models/initial_model")       # save model to this directory                               
                                                                                                

def main():
    cleaner = Cleaner()
    # Load codes and product names csv file
    codes, names = load_data("./data/save.csv")  

    # Remove punctuation from product names
    for name in range(len(names)):
        names[name] = str(names[name])

    names = cleaner.punct_sents(names) 

    # Create Patterns
    patterns = create_patterns(codes, names)
    # print(patterns)

    # Add patterns and create a model
    create_model(patterns)




if __name__ == '__main__':
    main()
