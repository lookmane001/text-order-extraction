"""Class of String Tokenization and cleaning functions"""
import spacy
import re
import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from spacy.lang.en import English

class Cleaner:

    # def __init__(self) -> None:
    #     pass

    def punct_sents(self, text):
        """Remove punctuation"""

        tp = type(text)
        if tp == type(list()):
            ls_words = text
        else:
            ls_words = text.split()
        for word in range(len(ls_words)):
            new_word = ""
            
            if ls_words[word].find(".") != len(ls_words[word])-1 and ls_words[word].find(".")>0:
                new_word = ls_words[word].replace(".","")
                ls_words[word] = new_word.strip()

            elif ls_words[word].find(".") >0 and word < len(ls_words)-1:
                new_word = ls_words[word].replace(".","")
                ls_words[word] = new_word.strip()
                
        text = (" ".join(ls_words))
        if tp == type(list()):
            return ls_words
        return (text)
  

    def single_sents(self, text):
        """split text into sentences"""

        sent_splitters = re.compile('[.!?]')
        sentences = sent_splitters.split(text)
        return (sentences)

        
    def tokenize(self, text_list):
        """Tokenizer data"""  
        tokenizer = RegexpTokenizer(r'\w+')
        dl = []
        for line in text_list:
            if len(line)>0:
                ls = []
                ls.append(" ".join(tokenizer.tokenize(line.lower().strip())))
                dl.append(ls)
        return (dl)


    
    def clean_stops(self, wordz):
        """remove english stop words"""
        stop_words = stopwords.words('english')
        tokenized_words = wordz[0].split()
        words = []
        for word in tokenized_words:
            if word not in stop_words:
                if len(word)>0:
                    words.append(word)
        return " ".join(words)

    
    def clean(self, text):
        """Clean text"""
        clean_text = self.single_sents(self.punct_sents(text))
        filtext = self.tokenize(clean_text)
        clean = self.clean_stops(filtext[0])
        return clean

  

