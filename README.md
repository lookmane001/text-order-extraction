# Text Order Extraction with Spacy

## Introduction
### Description

The aim of the project is to train a ML model to extract data from a text order by making using of SpaCy python library as well as Natural Language Toolkit libraries.

## Implementation
#### Prepare Patterns
In order to create patterns, the codes and names of products should be loaded to the main method of ```prepare_data.py ```. The ```./data/save.csv ``` is currently used to load the product codes and names. When this file is executed the patterns will be created using the codes and names and an initial model will be created.

#### Create Training Data
The training data is created by loading the codes and names of the products to the main method of ```training_data.py ```. Test data will be created and the intial model will be tested and the results will be save as ```TRAIN_DATA.json ``` and ```TEST_DATA.json ``` under the data directory.

#### Convert Training Data and Create Models
To train the final models the ```TRAIN_DATA.json ``` needs to be converted into a ```.spacy``` file and a ```config.cfg ``` needs to be created. This is done by executing the ```change_data_format.ipyb ```. Two final models will created; model-best and model-last.

#### Test Models
The final models can tested informally using the ```informal_test.py ```. A formal test is performed by executing ```formal_test.ipynb``` which uses the ```TEST_DATA.json ``` to test a model and returns a confusion matric for the model. Other test data can be loaded to test the models

## Dependacies

The following python modules were used:

- [SpaCy](https://pypi.org/project/spacy/)
- [nltk (Natural Language Toolkit)](https://pypi.org/project/nltk/)
- [numpy](https://pypi.org/project/numpy/)
- [pandas](https://pypi.org/project/pandas/)
- [Faker](https://pypi.org/project/Faker/)
- Regex
